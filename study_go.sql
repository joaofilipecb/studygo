-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 01-Nov-2018 às 19:37
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `study_go`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_alunos`
--

CREATE TABLE `tb_alunos` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `rg` int(8) NOT NULL,
  `idade` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_alunos`
--

INSERT INTO `tb_alunos` (`id`, `nome`, `rg`, `idade`) VALUES
(1, 'João', 208171005, 20),
(2, 'João Filipe', 127125, 21);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_atividades`
--

CREATE TABLE `tb_atividades` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `arquivo` longtext,
  `data` date NOT NULL,
  `fkCodAluno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avisos`
--

CREATE TABLE `tb_avisos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `fkCodAtividades` int(11) DEFAULT NULL,
  `fkCodAluno` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_avisos`
--

INSERT INTO `tb_avisos` (`id`, `titulo`, `descricao`, `data`, `fkCodAtividades`, `fkCodAluno`) VALUES
(242, 'Lorem Ipsum Dolor Sid Amet 4', 'Lorem Ipsum Dolor Sid Amet 3', NULL, NULL, 2),
(243, 'Lorem Ipsum Dolor Sid Amet', 'Lorem Ipsum Dolor Sid Amet', NULL, NULL, 2),
(244, 'Lorem Ipsum Dolor Sid Amet', 'Lorem Ipsum Dolor Sid Amet', NULL, NULL, 1),
(246, 'Lorem Ipsum Dolor Sid Amet 4', 'Lorem Ipsum Dolor Sid Amet 4\r\n', NULL, NULL, 2),
(247, 'Lorem Ipsum Dolor Sid Amet 5', 'Lorem Ipsum Dolor Sid Amet 5\r\n', NULL, NULL, 2),
(249, '2', 'as', NULL, NULL, 2),
(251, 'Aviso ', 'Ok\r\n', NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_notas`
--

CREATE TABLE `tb_notas` (
  `id` int(11) NOT NULL,
  `nota` int(20) NOT NULL,
  `fkCodAluno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_professores`
--

CREATE TABLE `tb_professores` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `sobrenome` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `senha` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_alunos`
--
ALTER TABLE `tb_alunos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rg` (`rg`);

--
-- Indexes for table `tb_atividades`
--
ALTER TABLE `tb_atividades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fkCodAluno` (`fkCodAluno`);

--
-- Indexes for table `tb_avisos`
--
ALTER TABLE `tb_avisos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fkCodAtividades` (`fkCodAtividades`),
  ADD KEY `fkCodAluno` (`fkCodAluno`);

--
-- Indexes for table `tb_notas`
--
ALTER TABLE `tb_notas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fkCodAluno` (`fkCodAluno`);

--
-- Indexes for table `tb_professores`
--
ALTER TABLE `tb_professores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_alunos`
--
ALTER TABLE `tb_alunos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_atividades`
--
ALTER TABLE `tb_atividades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_avisos`
--
ALTER TABLE `tb_avisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `tb_notas`
--
ALTER TABLE `tb_notas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_atividades`
--
ALTER TABLE `tb_atividades`
  ADD CONSTRAINT `fkCodAluno_fk[id]` FOREIGN KEY (`fkCodAluno`) REFERENCES `tb_alunos` (`id`);

--
-- Limitadores para a tabela `tb_avisos`
--
ALTER TABLE `tb_avisos`
  ADD CONSTRAINT `[OwnerName]_fk[num_for_dup]` FOREIGN KEY (`fkCodAtividades`) REFERENCES `tb_atividades` (`id`),
  ADD CONSTRAINT `[fkCodAluno]_fk[id]` FOREIGN KEY (`fkCodAluno`) REFERENCES `tb_alunos` (`id`);

--
-- Limitadores para a tabela `tb_notas`
--
ALTER TABLE `tb_notas`
  ADD CONSTRAINT `[id]_fk[fkCodAluno]` FOREIGN KEY (`fkCodAluno`) REFERENCES `tb_alunos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
