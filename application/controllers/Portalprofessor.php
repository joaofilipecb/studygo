<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Portalprofessor extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('professor', 'professor');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index() {
        $this->template->load('template', 'pagina_inicial');
        $this->load->view('portal_professor/pages/index');
    }

    public function cadAviso() {
        $this->template->load('template', 'portal_professor/pages/cad-aviso');
    }

    public function avisos() {
        $exibir_avisos = $this->professor->exibirAvisosByProfessor();
        $dados = array('exibiravisos' => $exibir_avisos);
        /* echo "<pre>";
          return print_r($dados);
          echo "</pre>"; */
        $this->template->load('template', 'portal_professor/pages/avisos', $dados);
    }

    public function avisoDestino(){
        $json = $this->professor->avisoDestino();
        $json_encode = json_encode($json);
        echo $json_encode;
    }

    public function editAviso($id_aviso) {
            $editar_aviso = $this->professor->editarAvisos($id_aviso);
            $dados = array('editaraviso' => $editar_aviso);
            $this->template->load('template', 'portal_professor/pages/edit-aviso', $dados);
    }

    public function atualizarAviso($id_aviso) {
        $dados = array(
            'titulo' => $this->input->post('titulo'),
            'descricao' => $this->input->post('descricao'),
            'aluno' => $this->input->post('aluno')
        );
        $this->professor->atualizarAviso($dados, $id_aviso);
    }

    public function excluirAviso($id_aviso) {
        $this->professor->excluirAviso($id_aviso);
    }

    public function enviarAviso() {
        $dados = array(
            'titulo' => $this->input->post('titulo'),
            'descricao' => $this->input->post('descricao'),
            'aluno' => $this->input->post('aluno')
        );
        $this->professor->cadastrarAvisos($dados);
    }

}
