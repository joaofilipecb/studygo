<?php

defined('BASEPATH')OR exit('No direct script access allowed');

class Professor extends CI_Model {

    public function exibirAvisosByAluno($id_aluno) {
        $sql = "SELECT * FROM tb_avisos av LEFT JOIN tb_alunos al ON av.fkCodAluno = al.id WHERE fkCodAluno = $id_aluno";
        $this->db->query("$query");
        return $sql->result();
    }

    public function addAluno(){
        $sql = "SELECT * FROM tb_alunos";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function exibirAvisosByProfessor() {
        $sql = "SELECT titulo, id, descricao FROM tb_avisos ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function avisoDestino(){
        $sql = "SELECT * FROM tb_alunos";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function editarAvisos($id_aviso) {
        $sql = "SELECT * FROM tb_avisos WHERE id = $id_aviso";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function atualizarAviso($dados, $id_aviso) {
        $sql = "UPDATE tb_avisos SET titulo = '$dados[titulo]', descricao = '$dados[descricao]', fkCodAluno = '$dados[aluno]' WHERE id = '$id_aviso'";
        $query = $this->db->query("$sql");
    }
    
    public function excluirAviso($id_aviso){
        $sql = "DELETE FROM tb_avisos WHERE id = $id_aviso"; 
        $query = $this->db->query($sql);
    }  
    
    public function cadastrarAvisos($dados) {
        $sql = "INSERT INTO tb_avisos (titulo, descricao, fkCodAluno) VALUES ('$dados[titulo]', '$dados[descricao]', '$dados[aluno]');";
        $this->db->query($sql);
    }

}
