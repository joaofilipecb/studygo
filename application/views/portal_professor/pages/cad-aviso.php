<div class="content-wrapper">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Cadastrar aviso</h4>
                        <?php echo validation_errors('<div></div>', '</div>'); ?>
                        <form id="cadastrar_aviso" action ="<?php echo base_url('portalprofessor/enviaraviso') ?>" method="post">
                            <div class="form-group">
                                <label for="exampleInputName1">Título</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Para quem é este aviso?</label>
                                <input type="text" class="form-control" id="aluno" name="aluno" placeholder="Digite um nome de um aluno...">
                            </div>
                            <div class="form-group">
                                <label for="exampleTextarea1">Descrição</label>
                                <textarea placeholder="Digite aqui a descrição do seu aviso" class="form-control" name="descricao" id="descricao" rows="2"></textarea>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Cadastrar aviso</button>
                            <a class="btn btn-light">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<footer class="footer">
    <div class="container-fluid clearfix">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
            <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
            <i class="mdi mdi-heart text-danger"></i>
        </span>
    </div>
</footer>
</div>
</div>
</div>
</body>

<script src="<?php echo base_url() ?>assets/portal-professor/vendors/js/vendor.bundle.base.js"></script>
<script src="<?php echo base_url() ?>assets/portal-professor/vendors/js/vendor.bundle.addons.js"></script>
<script src="<?php echo base_url() ?>assets/portal-professor/js/off-canvas.js"></script>
<script src="<?php echo base_url() ?>assets/portal-professor/js/misc.js"></script>

<script>
     $(document).ready(function () {
        $("body").on( "click", ".btn-success", function() {
            $.ajax({
                url: '<?php echo base_url(); ?>' + 'portalprofessor/enviaraviso',
                type: 'POST',
                data: $("#cadastrar_aviso").serialize(),
                success: function () {
                      // window.location.href = '<?php echo base_url(); ?>' + 'portalprofessor/avisos';
                      alert("Enviado com sucesso");
                }
            });
            return false;
        });
    });

</script>
</html>