
<div class="content-wrapper">
    <div class="row">
  <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                  <p>Tem certeza que deseja excluir este aviso?</p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger delete-aviso" data-dismiss="modal">Excluir</button>
              </div>
          </div>
      </div>
  </div>
        
        

        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Editar aviso</h4>
                    <?php echo validation_errors('<div class="alert alert-danger"></div>', '</div>'); ?>
                    <form id="atualizar_aviso" action ="<?php echo base_url('portalprofessor/atualizaraviso/' . $editaraviso[0]->id) ?>" method="post">
                        <div class="form-group">
                            <label for="exampleInputName1">Título</label>
                            <input type="text" value="<?php echo $editaraviso[0]->titulo ?>" class="form-control" id="titulo" name="titulo" placeholder="Título">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName1">Para quem é este aviso?</label>
                            <input type="text" value="<?php echo $editaraviso[0]->fkCodAluno ?>" class="form-control aluno" id="aluno" name="aluno" placeholder="Digite um nome de um aluno...">
                        </div>
                        <div class="form-group">
                            <label for="exampleTextarea1">Descrição</label>
                            <textarea placeholder="Digite aqui a descrição do seu aviso" class="form-control" name="descricao" id="descricao" rows="2"><?php echo $editaraviso[0]->descricao ?></textarea>
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Atualizar</button>
                        <a data-toggle="modal" data-target="#myModal" class="btn btn-danger">Excluir</a>
                    </form>
                </div>
            </div>
        </div>

        </form>
    </div>
</div>

 <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
      </div>
    </div>
  </div>
</body>


<script src="<?php echo base_url() ?>assets/portal-professor/vendors/js/vendor.bundle.base.js"></script>
<script src="<?php echo base_url() ?>assets/portal-professor/vendors/js/vendor.bundle.addons.js"></script>
<script src="<?php echo base_url() ?>assets/portal-professor/js/off-canvas.js"></script>
<script src="<?php echo base_url() ?>assets/portal-professor/js/misc.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        $.getJSON( "http://studygo.com.br/portalprofessor/avisodestino", function(data) {
            $.each( data, function( key, val ) {
                var aluno = val.nome;
                return aluno;
            });
        });

                 $( function() {
                var availableTags = [aluno];
                $( ".aluno" ).autocomplete({
                source: availableTags
            });
            });

        $("body").on( "click", ".btn-success", function() {
            $.ajax({
                url: '<?php echo base_url(); ?>' + 'portalprofessor/atualizaraviso/<?php echo $editaraviso[0]->id;?>',
                type: 'POST',
                data: $("#atualizar_aviso").serialize(),
                success: function () {
                    alert('Aviso atualizado com sucesso');
                }
            });
            return false;
        });

         $( "body" ).on( "click", ".delete-aviso", function() {
            $.ajax({
                url: '<?php echo base_url(); ?>' + 'portalprofessor/excluiraviso/<?php echo $editaraviso[0]->id; ?>',
                type: 'GET',
                data: $("#atualizar_aviso").serialize(),
                success: function (msg, data) {
                    if ($(".delete-aviso")) {
                        window.location.href = '<?php echo base_url(); ?>' + 'portalprofessor/avisos';
                    }
                }
            });
            return false;
        });
    });
</script>
</html>
