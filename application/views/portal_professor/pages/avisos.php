
        <div class="content-wrapper">
            <div class="mt-4 mb-4">
                <a href="<?php echo base_url() ?>portalprofessor/cadaviso">
                    <button type="button" class="btn btn-success btn-fw">
                        <i class="fa fa-plus-circle"></i>Novo aviso
                    </button>
                </a>
            </div>

            <div class="row">
                <?php foreach ($exibiravisos as $key => $valor) { ?>
                    <div class="col-md-4 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo $valor->titulo; ?></h4>
                                <div class="media">
                                    <i class="mdi mdi-earth icon-md text-info d-flex align-self-start mr-3"></i>
                                    <div class="media-body">
                                        <p class="card-text"><?php echo $valor->descricao; ?></p>
                                    </div>
                                </div>
                                <a class="float-center" href="<?php echo base_url('portalprofessor/editaviso/' . $valor->id) ?>">
                                    <button type="button" class="btn btn-info btn-block mt-3">Editar</button>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>


                </form>
            </div>
        </div>